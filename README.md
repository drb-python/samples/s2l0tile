# Sentinel-2 L0 Tiles

This project contains python source code using DRB python module to gather
Sentinel-2 L0 granules by L1C tiles.

This sample Drb python demonstration code aims to connect to OData service 
to select S2L0 MSI products matching a geometry filter representing a MGRS 
tile within a selected orbit.

A large part of the current code is dedicated to convert MGRS to WKT.
For Example, selection Tile `30TWT` for orbit `19725` is filtered by the 
following odata filter:

```
$filter=OData.CSC.Intersects(
   location=Footprint,
   area=geography'SRID=4326;Polygon((
      -3.0000000000000004 47.853449606531456,
      -1.6812546959200463 47.84571617334621,
      -1.681549571055185 46.945963417801224,
      -3.0 46.95352920137089,
      -3.0000000000000004 47.853449606531456))') and 
   Attributes/OData.CSC.StringAttribute/any(
       att:att/Name eq 'productType' and
       att/OData.CSC.StringAttribute/Value eq 'MSI_L0__GR') and 
   Attributes/OData.CSC.IntegerAttribute/any(
       att:att/Name eq 'orbitNumber' and 
       att/OData.CSC.IntegerAttribute/Value eq 19725)
```
This filter is composed of 3 parts:
= 
# Testings
A tests present in this module skips end-to-end tests cases that connects to
a CSC OData server. This test requires service credentials. By removing
`@unitest.skip` it is possible to retrieve granules related to the tile `30TWT`.

# Service API (_Draft_)
This sources also includes the implementation of a basic PaaS dedicated to 
provide these computations as a service.
***
<u><b>s2l0tile</b></u>:  Root of the service.<br/>
***
<u><b>/s2l0tile/help</u></b>: The online help.<br/>
***
<u><b>/s2l0tile/odata/auth</u></b>: This interface is used to install the 
OData service credentials. It can be call just once at the installation time,
then the credentials are locally stored into the local secure keyring.

__parameters__:<br/>
`service` : service url<br>
`username`: service username<br>
`password`: service password<br>

__example__:
```jsonpath
/s2l0tile/odata/auth?service=https://beta.databridge.gael-systems.com/gss-catalogue&username=user&password=xxxx 
```
This `/auth` service does not checks the validity of the credentials.

__Note1__: One service accepts only one credential(u/p), but more than one service could be registered via this API.<br/>
__Note2__: This API is not enough secure to transfer credentials. So the access to this service has been limited to local or debug connections.
***
<u><b>/s2l0tile/odata/auth/check</u></b>: Tool to check the configured authentication.

```jsonpath
/s2l0tile/odata/auth/check?service=https://beta.databridge.gael-systems.com/gss-catalogue
Ok

/s2l0tile/odata/auth/check?service=http://bad/service
Forbidden
Connection Error
```
***
<u><b>/s2l0tile/odata/filter</u></b>: Execute the OData filter to retrieve S2 L0 tiles:<br>

| parameter      | Optional<br/>Mandatory | description                                                                      |
|----------------|------------------------|----------------------------------------------------------------------------------|
| `<mgrs>`       | M                      | MGRS encoded tile to retrieve L0 datasets (i.e. `30TVU`)                         |
| `[start_date]` | O                      | Start date of acquisition in `ISO-8601` format (i.e. `2019-04-02T11:18:00.000Z`) |
| `[stop_date]`  | O                      | End date of acquisition in `ISO-8601` format (i.e. `2019-04-02T11:18:00.000Z`)   |
| `orbit`        | O                      | The orbit number to be selected (i.e. `19725`)                                   |
| `[top]`        | O                      | The number of expected results.                                                  |
| `[service]`    | O                      | The OData service to request.                                                    |


When the acquisition date is not provided, `start_date` is replaced by the Sentinel-2A launch date, and `stop_date` is replaced by the current date. The authentication to the OData service can be managed via /odata/auth service, this service does not allow to pass credentials.

__Syntax__
```jsonpath
/s2l0tile/odata/filter?mgrs=30TWT&orbit=19725
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(531faabe-e3a7-31c4-9818-79242e6939fa)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(edf08e72-a51c-3274-9685-3d5f69c19b3c)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(2e2c2d0b-9037-3365-840f-bc608f6de1d9)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(4c599207-38b6-362d-b325-161dd920ffe6)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(b5993665-1134-3ecf-9399-cf32d0eed8d3)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(53a4e147-7825-379c-ac17-ced5e629d0fb)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(88237866-f33a-3585-8e34-e6560eea12fc)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(fa52e9b4-3c5a-3ae8-9e04-7d84bb3d47b1)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(9f476bea-02eb-311d-a434-9f8ea3343b10)
 https://beta.databridge.gael-systems.com/gss-catalogue/Products(f1e7c91c-f6d9-32f5-a78a-595974024616)
```
***
<u><b>/s2l0tile/odata/filter_tiles</u></b>: Execute the OData filter to retrieve S2 L0 tiles:<br>

| parameter      | Optional<br/>Mandatory | description                                                                                                 |
|----------------|------------------------|-------------------------------------------------------------------------------------------------------------|
| `<mgrs>`       | M                      | MGRS encoded tile to retrieve L0 datasets (i.e. `30TVU`)                                                    |
| `[start_date]` | O                      | Start date of acquisition in `ISO-8601` format (i.e. `2019-04-02T11:18:00.000Z`)                            |
| `[stop_date]`  | O                      | End date of acquisition in `ISO-8601` format (i.e. `2019-04-02T11:18:00.000Z`)                              |
| `orbit`        | O                      | The orbit number to be selected (i.e. `19725`)                                                              |
| `[length]`     | O                      | The number of expected results.                                                                             |
| `[start]`      | O                      | The start index of the results: we skip the result before this index.                                       |
| `[order[0]]`   | O                      | Allow to order by start_date(column 2) or stop_date(column 3) example : order[0][column]=2&order[0][dir]=asc. |


When the acquisition date is not provided, `start_date` is replaced by the Sentinel-2A launch date, and `stop_date` is replaced by the current date. The authentication to the OData service can be managed via /odata/auth service, this service does not allow to pass credentials.

__Syntax__
```jsonpath
/s2l0tile/odata/filter_tiles?mgrs=30TDV&length=3&order[0][column]=2&order[0][dir]=desc
 {"elts": 
 [
 {"Name": "S2B_OPER_MSI_L0__GR_VGS2_20200929T124501_S20200929T104736_D11_N02.09.tar", "Strip": "S2B_OPER_MSI_L0__DS_VGS2_20200929T124501_S20200929T103918_N02.09", "Online": true, "Footprint": "geography'SRID=4326;Polygon((6.89223500598581 48.0317208308635,7.05276068692994 48.3911966185717,6.71590416860144 48.4659514975009,6.55738146285067 48.1071866943472,6.89223500598581 48.0317208308635))'", "Size": 18388992, "Cloud_Coverage": 95.5303, "Start": "2020-09-29T10:47:36.000Z", "Stop": "2020-09-29T10:47:36.000Z", "Orbit": 18624}, 
 {"Name": "S2B_OPER_MSI_L0__GR_VGS2_20200929T124501_S20200929T104732_D10_N02.09.tar", "Strip": "S2B_OPER_MSI_L0__DS_VGS2_20200929T124501_S20200929T103918_N02.09", "Online": true, "Footprint": "geography'SRID=4326;Polygon((6.55415662326758 48.0434289551519,6.71210569745917 48.4030327167338,6.37762349551165 48.4757567166503,6.22256297221163 48.1164797149333,6.55415662326758 48.0434289551519))'", "Size": 18388992, "Cloud_Coverage": 98.9804, "Start": "2020-09-29T10:47:32.000Z", "Stop": "2020-09-29T10:47:32.000Z", "Orbit": 18624}, 
 {"Name": "S2B_OPER_MSI_L0__GR_VGS2_20200929T124501_S20200929T104732_D11_N02.09.tar", "Strip": "S2B_OPER_MSI_L0__DS_VGS2_20200929T124501_S20200929T103918_N02.09", "Online": true, "Footprint": "geography'SRID=4326;Polygon((6.98724561527839 48.2403809949639,7.14823259009875 48.5999593132379,6.80979462654461 48.6749356037072,6.65005331852481 48.3162165458328,6.98724561527839 48.2403809949639))'", "Size": 18388992, "Cloud_Coverage": 99.6963, "Start": "2020-09-29T10:47:32.000Z", "Stop": "2020-09-29T10:47:32.000Z", "Orbit": 18624}
 ], 
 "conteur": {"Orbit": {"18624": 3}}}
 ```
***
<u><b>/s2l0tile/wkt</u></b>: Utility to compute MGRS formatted tile into lat/lon WKT format.

__Syntax__
```jsonpath
/s2l0tile/wkt?mgrs=30UUV&mgrs=30UVV&mgrs=30UWV&mgrs=30UXV&mgrs=30TUU&mgrs=30TVU
MULTIPOLYGON((-5.719358497550818 49.6209281250983,
              -4.3547350999437935 49.612691744014924,
              -4.35506577224682 48.71286351482557,
              -5.719358497550818 48.720914867739545,
              -5.719358497550818 49.6209281250983),
             (-4.360279246259113 49.644998628017, ...


/s2l0tile/wkt?mgrs=30UUV
POLYGON((-5.719358497550818 49.6209281250983, 
         -4.3547350999437935 49.612691744014924,
         -4.35506577224682 48.71286351482557,
         -5.719358497550818 48.720914867739545,
         -5.719358497550818 49.6209281250983))
```
***
<u><b>/s2l0tile/tiles</u></b>: Computes tiles coordinates in MGRS format intersecting latitude/longitude point and optionally its mgrs precision (__0:100km__, 1:10km, 2:1km, 3:100m, 4:10m, 5:1m)

__Syntax__
```jsonpath
/s2l0tile/tiles?latitude=48.8588336&longitude=2.2769949&precision=5
31UDQ4696812015

/s2l0tile/tiles?latitude=48.8588336&longitude=2.2769949&precision=0
31UDQ
```                                                                                                                                      

# Improvements
 - by removing the orbit filter part, it shall be possible to manage S2L0 granules  grouped by orbits. 
 - Manage a dedicated product format to gather granules by tiles.
 - Set an online service to select tile, orbit then retrieve the related product list to be downloaded.