from math import sin, cos, asin, atan2, pi, sqrt, radians, degrees
from datetime import datetime, timezone
from s2l0tile import mgrs


def earth_radius(latitude: float):
    """
    The earth is a flattened ellipsoid, the simplest way to manage distance at
    latitude is to compute a mean radius at the latitude of computation.
    :param latitude: the computation latitude
    :return: the mean radius at the passed latitude
    """
    # The ellipsoid
    a = 6378137.0
    b = 6356752.314245179497563967

    r1 = a
    r2 = b
    beta = latitude

    return sqrt((pow(pow(r1, 2) * cos(beta), 2) +
                 pow(pow(r2, 2) * sin(beta), 2)) /
                (pow(r1 * cos(beta), 2) + pow(r2 * sin(beta), 2)))


def translate(latitude_deg: float, longitude_deg: float, distance: float,
              bearing_deg: float):
    """
    Translate from lat/lon with a distance and a bearing in degrees.
    :param latitude_deg: origin latitude
    :param longitude_deg: origin longitude
    :param distance: distance to move (m)
    :param bearing_deg: bearing 0 stats among lat axis.
    :return: lat/lon in degrees of the translated position. To avoid any
        confusion, values are stored into dictionary structure indexed
        with `lat` and `lon` keys.
    """
    latitude = radians(latitude_deg)
    longitude = radians(longitude_deg)
    bearing = radians(bearing_deg)
    r = earth_radius(latitude)
    lat = asin(
        sin(latitude) * cos(distance / r) +
        cos(latitude) * sin(distance / r) * cos(bearing))

    lon = longitude + atan2(
        sin(bearing) * sin(distance / r) * cos(latitude),
        cos(distance / r) - sin(latitude) * sin(latitude))

    return {'lat': degrees(lat),
            'lon': degrees(lon)}


def mgrs_box(mgrs_string: str):
    """
    Compute box representing an MGRS area.
    :param mgrs_string: the MGRS areas
    :return: return corner coordinates of the box as dictionary indexed by keys
        `ul` as upper left, `ur` as upper right, `ll` as lower left,
        `lr` as lower right.
    """
    utm = mgrs.decode(mgrs_string)
    accuracy = utm['accuracy']
    ll = mgrs.UTMtoLL(utm)
    ul = translate(ll['lat'], ll['lon'], accuracy, 0)
    ur = translate(ll['lat'], ll['lon'], sqrt(2*accuracy*accuracy), 45)
    lr = translate(ll['lat'], ll['lon'], accuracy, 90)
    return {
        "ul": {'lon': ul['lon'], 'lat': ul['lat']},
        "ur": {'lon': ur['lon'], 'lat': ur['lat']},
        "lr": {'lon': lr['lon'], 'lat': lr['lat']},
        "ll": {'lon': ll['lon'], 'lat': ll['lat']}}


def box_to_wkt_content(box):
    """Prepare box to be used into a WKT geometry, repeating first/last corner.
    :param box: the ul/ur/lr/ll box
    :return: the WKT string list of points
    """
    return \
        f"({box['ul']['lon']} {box['ul']['lat']}," \
        f"{box['ur']['lon']} {box['ur']['lat']}," \
        f"{box['lr']['lon']} {box['lr']['lat']}," \
        f"{box['ll']['lon']} {box['ll']['lat']}," \
        f"{box['ul']['lon']} {box['ul']['lat']})"


def odata_geo_filter_for_mgrs_tile(tile: str):
    return\
        f"OData.CSC.Intersects(location=Footprint,area=geography'SRID=4326;" \
        f"Polygon({box_to_wkt_content(mgrs_box(tile))})')"


def odata_s2_l0_granule_filter():
    return "Attributes/OData.CSC.StringAttribute/any(" \
           "att:att/Name eq 'productType' and " \
           f"att/OData.CSC.StringAttribute/Value eq 'MSI_L0__GR')"


def odata_attr_int_filter_eq(name_attr: str, value):
    return "Attributes/OData.CSC.IntegerAttribute/any(" \
           f"att:att/Name eq '{name_attr}' and " \
           f"att/OData.CSC.IntegerAttribute/Value eq {value})"


def odata_attr_int_filter_le(name_attr: str, value):
    return "Attributes/OData.CSC.IntegerAttribute/any(" \
           f"att:att/Name eq '{name_attr}' and " \
           f"att/OData.CSC.IntegerAttribute/Value le {value})"


def odata_online_filter(online: bool):
    if online:
        return "Online eq true"
    else:
        return "Online eq false"


def odata_online_filter(online: bool):
    if online:
        return "Online eq true"
    else:
        return "Online eq false"


def odata_sensing_date(start: datetime, stop: datetime):
    if start is None or stop is None:
        raise ValueError("Sending start or stop are missing.")
    start_str = start.astimezone(timezone.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
    stop_str = stop.astimezone(timezone.utc).strftime('%Y-%m-%dT%H:%M:%SZ')
    return \
        f"ContentDate/Start ge {start_str} and ContentDate/End le {stop_str}"


def odata_filter_date(start: datetime, stop: datetime):
    if start is None or stop is None:
        raise ValueError("Sending start or stop are missing.")
    start_str = start.strftime('%Y-%m-%dT%H:%M:%SZ')
    stop_str = stop.strftime('%Y-%m-%dT%H:%M:%SZ')
    return \
        f"ContentDate/Start ge {start_str} and ContentDate/End le {stop_str}"
