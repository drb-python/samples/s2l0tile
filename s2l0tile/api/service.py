import io
import json
import re
import uuid
from werkzeug.wsgi import FileWrapper

import sys

import keyring
from drb_impl_odata.odata_utils import OdataServiceType
from keyring.errors import NoKeyringError
from keyrings.cryptfile.cryptfile import CryptFileKeyring

from flask import Flask, request, send_file, abort, Response

from requests.auth import HTTPBasicAuth
from drb_impl_odata import ODataQueryPredicate, ODataServiceNode

from datetime import datetime
import logging
import importlib.resources as rs
import dateutil.parser
from flask_cors import CORS
import os

sys.path.append('../..')  # noqa
from s2l0tile import s2l0tile, mgrs
from s2l0tile import api

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
LOGGER = logging.getLogger('S2L0-Tile')
LOGGER.info(f'Starting S2L0 Tile service.')

init = False


class ResultOdata:

    def __init__(self):
        self.elts = []
        self.counter = {'Orbit': {}}


def _log_kr_info(verbose: bool):
    if not verbose:
        return
    try:
        kr = keyring.get_keyring()
        LOGGER.info(f'Using keyring backend {type(kr)}.')
    except Exception:
        LOGGER.info(f'Keyring not prperly set.')


def _init_keyring_backed(verbose: bool):
    global init
    if init:
        _log_kr_info(verbose)
        return
    try:
        keyring.get_keyring()
        raise NoKeyringError()
    except NoKeyringError:
        kr = CryptFileKeyring()
        kr.keyring_key = 'a25Za/.?$'  # Warn: Same as PlaintextKeyring
        keyring.set_keyring(kr)
    _log_kr_info(verbose)
    init = True


def _get_total_count(
        service_adr='https://beta.databridge.gael-systems.com/gss-catalogue'):
    service = request.args.get('service', os.environ.get(
        'gss_service',
        'https://beta.databridge.gael-systems.com/gss-catalogue'))

    cred = keyring.get_credential(service_name=service, username=None)
    if cred is None or \
            cred.password is None or \
            cred.username is None:
        username = os.environ.get('gss_username', "user-guest")
        password = os.environ.get('gss_password', "qQR@!4nL4Uit")

        # abort(401, f"Please use /odata/auth to set credentials {service}")
    else:
        username = cred.username
        password = cred.password

    svc = ODataServiceNode(service, HTTPBasicAuth(username, password))
    return json.dumps(len(svc.children))


def _get_service():
    service = request.args.get('service', os.environ.get(
        'gss_service',
        'https://beta.databridge.gael-systems.com/gss-catalogue'))

    cred = keyring.get_credential(service_name=service, username=None)
    if cred is None or \
            cred.password is None or \
            cred.username is None:
        username = os.environ.get('gss_username', "user-guest")
        password = os.environ.get('gss_password', "qQR@!4nL4Uit")

        # abort(401, f"Please use /odata/auth to set credentials {service}")
    else:
        username = cred.username
        password = cred.password

    svc = ODataServiceNode(service, HTTPBasicAuth(username, password))

    return svc


def _get_int_arg(value):
    value_int = None
    if isinstance(value, str) and len(value) > 0:
        value_int = int(value)
    elif isinstance(value, int):
        value_int = value
    return value_int


def _odata_filter(tile, orbit=None,
                  start_date: datetime = datetime(year=1900,
                                                  month=0o1, day=0o1),
                  stop_date: datetime = datetime.utcnow(),
                  order='ContentDate/Start asc',
                  online=None,
                  cloud_coverage=None):
    if tile is None:
        abort(500, description="MGRS tile must be set (i.e. mgrs=30TDV).")

    svc = _get_service()

    LOGGER.debug(f"Acquisition stop  = {stop_date}")

    filters = [s2l0tile.odata_s2_l0_granule_filter(),
               s2l0tile.odata_filter_date(start_date, stop_date),
               s2l0tile.odata_geo_filter_for_mgrs_tile(tile)]

    if orbit is not None:
        orbit_int = _get_int_arg(orbit)
        if orbit_int is not None:
            filters.append(
                s2l0tile.odata_attr_int_filter_eq('orbitNumber', orbit_int))

    if cloud_coverage is not None:
        cloud_coverage_int = _get_int_arg(cloud_coverage)
        if cloud_coverage_int is not None:
            filters.append(
                s2l0tile.odata_attr_int_filter_le('cloudCover',
                                                  cloud_coverage_int))

    if online is not None:
        if isinstance(online, str):
            if online.lower() == 'true':
                online = True
            elif online.lower() == 'false':
                online = False
        if isinstance(online, bool):
            filters.append(s2l0tile.odata_online_filter(online))

    _filter = _fix_encoding(" and ".join(filters))
    LOGGER.debug(f"Computed filter = {_filter}")

    predicate = ODataQueryPredicate(filter=_filter,
                                    order=order)
    return svc, predicate


with rs.path(api, 'help.html') as file:
    help_file = file

app = Flask('S2L0 Tile Service')

app.config["DEBUG"] = True
app.context = {}
app.context['recordsTotal'] = None


@app.route('/s2l0tile/help')
def help():
    return send_file(help_file)


@app.route('/s2l0tile/tiles')
def tiles():
    latitude = request.args.get('latitude')
    longitude = request.args.get('longitude')
    precision = request.args.get('precision')
    if latitude is None or longitude is None:
        return f'<b>Use syntax {request.base_url}?' \
               f'latitude=48.1&longitude=2.54</b>'
    if precision is None:
        precision = 0
    return mgrs.encode(mgrs.LLtoUTM(float(latitude), float(longitude)),
                       int(precision))


@app.route('/s2l0tile/wkt')
def wkt():
    mgrs_coords = request.args.getlist('mgrs')
    if mgrs_coords is None or len(mgrs_coords) == 0:
        return f'<b>Use syntax {request.base_url}?' \
               f'mgrs=31UDQ/b>'
    cs = [s2l0tile.box_to_wkt_content(s2l0tile.mgrs_box(coord))
          for coord in mgrs_coords]
    if len(cs) > 1:
        return f'MULTIPOLYGON({",".join(cs)})'
    return f'POLYGON({cs[0]})'


def _fix_encoding(filter: str):
    return filter.replace('\'', '%27')


@app.route('/s2l0tile/odata/auth')
def auth():
    """
    OData Service connection management:
      Odata service connection can be fully configured via this REST API.
      Such an API is not enough secure to transfer credentials and the
      system has been limited to local or debug connections.
      Credential are stored into the system keyring. It is not required to
      provide them again once set.

      Note that one service accepts only one credential.
      Multiple service is properly handled with their credentials.

      Credentials could also be provided from os system environment variables:
       - gss_service
       - gss_username
       - gss_password
       Or they could be added manually into the system keyring when accessible.
       This /auth service does not checks the validity of the credentials.
    """
    if not app.config.get('DEBUG') and request.remote_addr != '127.0.0.1':
        abort(403)

    service = request.args.get('service', os.environ.get(
        'gss_service',
        'https://beta.databridge.gael-systems.com/gss-catalogue'))

    username = request.args.get('username', os.environ.get('gss_username',
                                                           "user-guest"))
    password = request.args.get('password', os.environ.get('gss_password',
                                                           "qQR@!4nL4Uit"))

    cred = keyring.get_credential(service_name=service, username=username)
    if cred is None and password is None:
        return "No credential passed."

    if cred is None or (password and cred.password != password):
        LOGGER.debug(f'Set New Creds {cred}: {username}/{password}')
        keyring.set_password(service_name=service,
                             username=username,
                             password=password)
        return "Credential modified."
    return "Credential not changed."


@app.route('/s2l0tile/odata/auth/check')
def check_auth():
    service = request.args.get('service', os.environ.get(
        'gss_service',
        'https://beta.databridge.gael-systems.com/gss-catalogue'))

    try:
        cred = keyring.get_credential(service_name=service, username=None)
        auth = HTTPBasicAuth(cred.username, cred.password)
        svc = ODataServiceNode(service, auth)
        if svc.type_service != OdataServiceType.CSC:
            raise ValueError("Bad service: {svc.type_service.name}")
    except Exception as e:
        LOGGER.warning("Service connection fails", e)
        abort(403, description="Connection Error")
    return "Ok"


def _convert_node_to_dict(node):
    result_dict = {'Name': node.get_attribute('Name', None),
                   'Strip': node['Attributes']['datastripId'].value}
    result_dict['Online'] = node.get_attribute('Online', None)
    footprint = node.get_attribute('Footprint', None)
    result_dict['Footprint'] = footprint

    m = re.search('geography\'SRID=4326;Polygon\(\((.+?)\)\)\'', footprint)  # noqa
    polygon_ll = []
    if m:
        polygon = m.group(1)
        coords_polygon = polygon.split(',')
        for coord in coords_polygon:
            lon_lat = coord.split()
            lat_lon = [lon_lat[1], lon_lat[0]]

            polygon_ll.append(lat_lon)
    result_dict['Polygon'] = polygon_ll

    result_dict['Size'] = node.get_attribute('ContentLength', None)
    result_dict['Cloud_Coverage'] = node['Attributes']['cloudCover'].value
    result_dict['Id'] = node.get_attribute('Id', None)

    # result['Coordinates'] = node['Attributes']['coordinates'].value
    content_date = node.get_attribute('ContentDate', None)
    result_dict['Start'] = content_date['Start']
    result_dict['Stop'] = content_date['End']

    orbit = node['Attributes']['orbitNumber'].value
    result_dict['Orbit'] = orbit
    return result_dict


def _convert_node_to_result(list_nodes):
    results = ResultOdata()

    for node in list_nodes:
        result = _convert_node_to_dict(node)

        orbit = result['Orbit']
        if orbit not in results.counter['Orbit'].keys():
            results.counter['Orbit'][orbit] = 1
        else:
            results.counter['Orbit'][orbit] = \
                results.counter['Orbit'][orbit] + 1

        results.elts.append(result)
    return results


def _filter_node(request):
    orbit = None
    tile = None
    start_date = datetime(year=1900, month=0o1, day=0o1)
    stop_date = datetime.utcnow()
    order_colum = 'ContentDate/Start'
    cloud_coverage = None

    online = None
    if 'online' in request.values:
        online = request.values.get('online')

    if 'cloud_coverage' in request.values:
        cloud_coverage = request.values.get('cloud_coverage')

    if 'orbit' in request.values:
        orbit = request.values.get('orbit')
    if 'mgrs' in request.values:
        tile = request.values.get('mgrs')

    if 'stop_date' in request.values:
        stop_date = dateutil.parser.parse(request.values['stop_date'],
                                          ignoretz=True)

    if 'start_date' in request.values:
        start_date = dateutil.parser.parse(request.values['start_date'],
                                           ignoretz=True)

    if 'order[0][dir]' in request.values:
        order_dir = request.values['order[0][dir]']

    if order_dir.lower() == 'asc':
        order_dir = 'asc'

    if 'order[0][column]' in request.values:
        colum_index = request.values['order[0][column]']
        if colum_index == '2' or colum_index == 2:
            order_colum = 'ContentDate/Start'
        elif colum_index == '3' or colum_index == 3:
            order_colum = 'ContentDate/End'

    order = order_colum + ' ' + order_dir

    svc, predicate = _odata_filter(tile, orbit,
                                   start_date=start_date,
                                   stop_date=stop_date,
                                   order=order,
                                   online=online,
                                   cloud_coverage=cloud_coverage)
    return svc, predicate


@app.route('/s2l0tile/odata/filter_tiles', methods=['GET'])
def filter_tiles():
    draw = '1'
    if 'draw' in request.values:
        draw = request.values.get('draw')

    svc, predicate = _filter_node(request)
    top = request.values.get('length', 10)
    skip = request.values.get('start', 0)
    skip = int(skip)
    top = int(top)

    def generate():
        """Generates the list of retrieved S2L0 products to display
           This method shall be improved to better match m2m connections.

           A dedicated service could generates S2L0 Tile datasets.
        """
        first = True
        yield '{'
        yield '"elts": ['

        list_value = svc[predicate]
        count = len(list_value)
        for s2l0 in list_value[skip:top+skip]:
            result = _convert_node_to_dict(s2l0)
            if not first:
                yield ", "
            else:
                first = False
            yield json.dumps(result)
        yield '], '

        if not first:
            yield '"recordsFiltered": ' + str(count)
        else:
            yield '"recordsFiltered": ' + str(0)

        if app.context['recordsTotal'] is None:
            app.context['recordsTotal'] = len(svc)
        yield ', "recordsTotal": ' + str(app.context['recordsTotal'])
        yield ', "draw": ' + str(draw)
        yield '}'

    return app.response_class(generate(), mimetype='application/json')


@app.route('/s2l0tile/odata/count_total')
def odata_count_total():
    return _get_total_count()


@app.route('/s2l0tile/odata/filter')
def odata_filter():
    tile = request.args.get('mgrs')
    LOGGER.debug(f"Tile MGRS  = {tile}")
    orbit = request.args.get('orbit')
    LOGGER.debug(f"Orbit  = {orbit}")

    top = request.args.get('top', 5)
    skip = request.args.get('skip', 0)
    top = int(top)
    skip = int(skip)

    start_date = request.args.get(
        'start_date', default="1900-06-23T00:00:00.000Z")
    stop_date = request.args.get(
        'stop_date', default=datetime.utcnow().isoformat())

    stop_date = dateutil.parser.isoparse(stop_date)
    start_date = dateutil.parser.isoparse(start_date)

    svc, predicate = _odata_filter(
        tile, start_date=start_date, stop_date=stop_date)

    def generate():
        """Generates the list of retrieved S2L0 products to display
           This method shall be improved to better match m2m connections.

           A dedicated service could generates S2L0 Tile datasets.
        """
        for s2l0 in svc[predicate][skip:top+skip]:
            yield s2l0.path.name + "\n"

    return app.response_class(generate(), mimetype='text/plain')


@app.route('/s2l0tile/odata/download', methods=['GET'])
def download():
    id = request.args.get('id')
    LOGGER.debug(f"Download Product {id}")

    svc = _get_service()
    uu_id = uuid.UUID(id)

    product = svc[uu_id]
    file_name = product.get_attribute('Name', None)
    io_download = product.get_impl(io.BytesIO)
    # test = ioBuffer.read()
    io_buf = io.BytesIO(io_download.getvalue())
    wrapper = FileWrapper(io_buf)
    headers = {
        'Content-Disposition': 'attachment; filename="{}"'.format(file_name)
    }

    return Response(wrapper,
                    headers=headers,
                    direct_passthrough=True,
                    mimetype="application/tar")


CORS(app)
app.run()
