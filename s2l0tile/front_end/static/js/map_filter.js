

    function get_parameter(params_dict) {
        params_dict.mgrs = $('#MGRS_id').val();
        if ( $('#ONLINE_id').is(':checked') ) {
            params_dict.online = true;
        }

        cloud_coverage = $('#COVERAGE_id').val();
        if (cloud_coverage.length  > 0)
        {
            params_dict.cloud_coverage = cloud_coverage;
        }

        orbit = $('#ORBIT_id').val();
        if (orbit.length  > 0)
        {
            params_dict.orbit = orbit;
        }
        start_date = $('#START_DATE_VALUE_id').val();
        if (start_date.length  > 0)
        {
            params_dict.start_date = start_date;
        }
        stop_date = $('#STOP_DATE_VALUE_id').val();
        if (stop_date.length > 0)
        {
            params_dict.stop_date = stop_date;
        }
    }



  function loadTable (value) {
       table = $('#dataId').DataTable();
       if (table)
       {
            page_length = table.page.len()
       }
       table.clear()
       table.destroy();
       updateTable(true);
   }

    function reset_polygon()
    {
        for (const [id, polygon] of Object.entries(polygon_show)) {
            polygon.remove()
        }
        polygon_show = {}
    }

    function check_field() {

        var regex_mgrs = /^[a-zA-Z0-9]+$/;
        var mgrs = $('#MGRS_id').val();
         if (mgrs.length  > 0)
        {

            if (!mgrs.match(regex_mgrs))
            {
                alert("MGRS must be alphanumeric only");
                return false;
            }
        }
        else
        {
          alert("MGRS is mandatory");
          return false;
        }

        var r = /^[0-9]+$/;
        var orbit = $('#ORBIT_id').val();
        if (orbit.length  > 0)
        {

            if (!orbit.match(r))
            {
                alert("Orbit must be numeric only");
                return false;
            }
        }

        var cloud_coverage = $('#COVERAGE_id').val();
        if (cloud_coverage.length  > 0)
        {
            if (!cloud_coverage.match(r))
            {
                alert("cloud coverage must be numeric only");
                return false;
            }
        }

        return true;

    }

    function updateTable (load_ajax) {

       reset_polygon();

       var config = {
        "order": [[ 2, "desc" ]],
        "scrollX": true,
        "pageLength": page_length,
        "searching" : false,

        columns: [

          {data: 'Name', orderable: false, searchable: false},
          {data: 'Orbit', orderable: false, searchable: false},
          {data: 'Start', orderable: true, searchable: false},
          {data: 'Stop', orderable: true, searchable: false},
          {data: 'Online', orderable: false, searchable: false},
          {data: 'Size', orderable: false, searchable: false},
          {data: 'Cloud_Coverage', orderable: false, searchable: false},
           {
                data:   "Select", orderable: false, searchable: false,
                render: function ( data, type, row ) {
                   if (row['Online'] === true) {
                   return '<button type="submit" class="btn btn-xs btn-primary" onclick="download_product_event(event, \'' + row['Id'] + '\',\'' + row['Name'] + '\')">Download</button>';
                   }
                   return '';
                },
                className: "dt-body-center"
            },

            {data: 'Polygon',  'visible': false}

        ],
      }

       if (load_ajax){
        config.serverSide = true;

       config.processing = true;
       config.ajax = {
            "url": "/data",
            "type": "GET",
            "dataType": "json",
            "rowId": 'Id',
            "data" : function ( d ) {
                get_parameter(d)
            },
            "dataSrc": function ( json ) {

                   fn_end_list()

                   return json.elts;
             }
        }
        }
        else
        {
              config.serverSide = false;

            config.processing = false;
        }

      table = $('#dataId').DataTable(config);
         $('#dataId tbody').on( 'click', 'tr', function (event) {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
            var id = this._DT_RowIndex;
            var rows = table.rows().data();

            if (id in polygon_show)
            {
                polygon = polygon_show[id];
                polygon.remove();
                delete polygon_show[id];
                $(this).removeClass('selected');
            }
            else
            {
                row = rows[id]
                var latlngs = row['Polygon'];

                //Create a polygon using the L.polygon(). Pass the locations/points as variable to draw the polygon, and an option to specify the color of the polygon.
                var polygon = L.polygon(latlngs, {color: 'red'});

                polygon.addTo(map)

                polygon_show[id]=polygon
                 $(this).addClass('selected');
            }

            var download_elts_enable = 0;

            for (id of Object.keys(polygon_show))
            {
                row = rows[id]
                if (row['Online'] === true)
                {
                    download_elts_enable++;
                }

            }


            show_download(download_elts_enable)

        });
     }

    function show_download(show) {
          var x = document.getElementById("DOWNLOAD_BLOCK_id");
          if (show > 0) {
            var btn = document.getElementById("DOWNLOAD_id");
            if (show === 1) {
                btn.innerText = "Download " + show  + " product"
            }
            else
            {
               btn.innerText = "Download " + show  + " products"
            }
            x.style.display = "block";
          } else {
            x.style.display = "none";
         }
     }

    function fn_end_list() {
       reset_polygon();
       $("#myCollapse").collapse("show");
       show_download(0)
    }

    function download_product_event(event, id, name) {
        event = event || window.event;
        if (event)
        {
            event.preventDefault();
            event.stopPropagation();
            event.stopImmediatePropagation();
        }
        download_product(id, name);
    }

    function download_list_elts(list_elts) {
        for ( var i = 0; i < list_elts.length; i++ ) {
            download_product(list_elts[i].Id, list_elts[i].Name);
        }
    }

    function download_all() {
       var table = $('#dataId').DataTable();
        var rows = table.rows().data();
         for ( var i = 0; i < rows.length; i++ ) {
            row = rows[i]
            if (row['Online'] === true) {
                var id = i;
                if (id in polygon_show)
                {
                   download_product(row['Id'], row['Name']);
                }

            }
        }

    }


