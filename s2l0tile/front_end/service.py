import random
from flask import render_template
from flask import Flask, request
import logging
from flask_bootstrap import Bootstrap
import requests
from flask_caching import Cache

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
LOGGER = logging.getLogger('S2L0-Tile')
LOGGER.info(f'Starting S2L0 Tile service.')

init = False


class ResultOdata:

    def __init__(self):
        self.elts = []
        self.counter = {}
        self.counter['Orbit'] = {}
        self.counter['Product Type'] = {}


app = Flask('S2L0 Tile Service', static_folder='./static')

app.config["DEBUG"] = True
app.config["CACHE_TYPE"] = "SimpleCache"
app.config["CACHE_DEFAULT_TIMEOUT"] = 300
cache = Cache(app)

app.context = {}
app.context['mgrs'] = "30TDV"


def random_html_color():
    r = random.randint(0, 256)
    g = random.randint(0, 256)
    b = random.randint(0, 256)
    return '#%02x%02x%02x' % (r, g, b)


@app.route('/')
# @cache.cached(timeout=10000
def demo():
    tile = request.args.get('mgrs')
    if tile is None and 'mgrs' in app.context.keys():
        tile = app.context['mgrs']

    return render_template('map_filter.html')


@app.route('/data', methods=["GET"])
def data():
    # redirect to api server
    ret = requests.get("http://localhost:5000/s2l0tile/odata/filter_tiles",
                       params=request.values)
    return ret.content


@app.route('/download', methods=["GET"])
def download():
    # redirect to api server
    ret = requests.get("http://localhost:5000/s2l0tile/odata/download",
                       params=request.values)
    return ret.content


Bootstrap(app)

app.run(port=5010)
