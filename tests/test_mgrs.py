import os
import unittest
from s2l0tile import s2l0tile
from s2l0tile.mgrs import UTMtoLL, decode, LLtoUTM, encode
from requests.auth import HTTPBasicAuth
import drb_impl_odata as odata
import dateutil.parser
from osgeo import ogr


def _box_to_wkt_content(box):
    return \
        f"({box['ul']['lon']} {box['ul']['lat']}," \
        f"{box['ur']['lon']} {box['ur']['lat']},"  \
        f"{box['lr']['lon']} {box['lr']['lat']},"  \
        f"{box['ll']['lon']} {box['ll']['lat']},"  \
        f"{box['ul']['lon']} {box['ul']['lat']})"


def _odata_sample_filter():
    start = dateutil.parser.isoparse("2019-04-02T11:17:00.000Z")
    stop = dateutil.parser.isoparse("2019-04-02T11:18:00.000Z")
    return f"{s2l0tile.odata_geo_filter_for_mgrs_tile('30TWT')} and " \
           f"{s2l0tile.odata_s2_l0_granule_filter()} and " \
           f"{s2l0tile.odata_obit_filter(19725)} and " \
           f"{s2l0tile.odata_sensing_date(start, stop)}"


def _fix_encoding(filter: str):
    return filter.replace('\'', '%27')


class TestMGRS(unittest.TestCase):
    """
    MGRS is a alternative representation, based on one line, of earth
    locations in UTM projection.
    This test aims to validate the MGRS accuracy of this library wrt  the
     notation chosen in sentinel-2 naming conventions.
    """
    def test_mgrs_parse(self):
        t = decode('39QVV')
        self.assertEqual(t['zoneLetter'], 'Q')
        self.assertEqual(t['easting'], 400000)
        self.assertEqual(t['northing'], 1900000)
        self.assertEqual(t['zoneNumber'], 39)
        self.assertEqual(t['accuracy'], 100000)
        print(t)

        t = decode('30TEN')
        self.assertEqual(t['zoneLetter'], 'T')
        self.assertEqual(t['easting'], 1300000.0)
        self.assertEqual(t['northing'], 4700000.0)
        self.assertEqual(t['zoneNumber'], 30)
        self.assertEqual(t['accuracy'], 100000)
        print(t)

    def test_compute_lon_lat_mgrs(self):
        # France/Morbihan/Plumergat
        # mgrs = '30TWT0527087568'
        mgrs = '30TWT'
        ll = UTMtoLL(decode(mgrs))
        self.assertAlmostEqual(ll['lat'], 47.741464, delta=1)
        self.assertAlmostEqual(ll['lon'], -2.929702, delta=1)
        self.assertEqual(encode(LLtoUTM(47.741464, -2.929698), 0), "30TWT")
        print(f'lat_lon({mgrs})={ll}')

    def test_mgrs_box(self):
        boxes = [
            s2l0tile.mgrs_box('30UUV'), s2l0tile.mgrs_box('30UVV'),
            s2l0tile.mgrs_box('30UWV'), s2l0tile.mgrs_box('30UXV'),
            s2l0tile.mgrs_box('30UYV'), s2l0tile.mgrs_box('30UZV'),

            s2l0tile.mgrs_box('30TUU'), s2l0tile.mgrs_box('30TVU'),
            s2l0tile.mgrs_box('30TWU'), s2l0tile.mgrs_box('30TXU'),
            s2l0tile.mgrs_box('30UYU'), s2l0tile.mgrs_box('30UZU'),

            s2l0tile.mgrs_box('30TUT'), s2l0tile.mgrs_box('30TVT'),
            s2l0tile.mgrs_box('30TWT'), s2l0tile.mgrs_box('30TXT'),
            s2l0tile.mgrs_box('30TYT'), s2l0tile.mgrs_box('30TZT'),

            s2l0tile.mgrs_box('30TUS'), s2l0tile.mgrs_box('30TVS'),
            s2l0tile.mgrs_box('30TWS'), s2l0tile.mgrs_box('30TXS'),
            s2l0tile.mgrs_box('30TYS'), s2l0tile.mgrs_box('30TZS'),

        ]

        wkt_polygon_list = ''
        for box in boxes:
            wkt_polygon_list += _box_to_wkt_content(box)
            if box != boxes[-1]:
                wkt_polygon_list += ','

        print(f"MULTIPOLYGON (({wkt_polygon_list}))")

    def test_date_range_box(self):
        start = dateutil.parser.isoparse("2017-08-03T09:02:25.000Z")
        stop = dateutil.parser.isoparse("2017-08-03T09:02:29.000Z")
        print(s2l0tile.odata_sensing_date(start, stop))
        self.assertEqual(
            "ContentDate/Start ge 2017-08-03T09:02:25Z and "
            "ContentDate/End le 2017-08-03T09:02:29Z",
            s2l0tile.odata_sensing_date(start, stop))

    def _footprint_to_geo(self, footprint: str) -> ogr.Geometry:
        """Manages WKT item geography returned by OData API.
        The method parses the input string footprint and convert it to OGR
        feature. When a geometry is defined, it is included as the SRS
        reference of the feature.

        Returns:
        ------
        ogr.Geometry
           The geometry shape representing the item footprint.

        Raises
        ------
        ValueError
           When the footprint string format cannot be parsed.
        """
        if footprint.startswith("geography'SRID="):
            srid, wkt = footprint.split('\'')[1].split(';')
            epsg = int(srid.split("=")[1])
            # srs = osr.SpatialReference()
            # srs.ImportFromEPSG(epsg)
            # geometry = ogr.CreateGeometryFromWkt(wkt, reference=srs)
            geometry = ogr.CreateGeometryFromWkt(wkt)
            return geometry
        else:
            raise ValueError(f"Unknown footprint format: {footprint}")

    @unittest.skip("Requires remote connection")
    def test_odata_service(self):
        service = os.environ.get(
            'gss_service',
            'https://beta.databridge.gael-systems.com/gss-catalogue')
        username = os.environ.get('gss_username')
        password = os.environ.get('gss_password')

        auth = HTTPBasicAuth(username, password)
        svc = odata.ODataServiceNode(service, auth)

        predicate = odata.ODataQueryPredicate(
            filter=_fix_encoding(_odata_sample_filter()),
            top=50, order='ContentLength asc')

        print(f"$filter={_odata_sample_filter()}")

        count = 1
        footprints = ogr.Geometry(ogr.wkbMultiPolygon)
        for product in svc[predicate]:
            print(f"{count}- {product.name}")
            attributes = product['Attributes']
            self.assertEqual(attributes['orbitNumber'].value, 19725, )
            footprints.AddGeometry(self._footprint_to_geo(
                product.get_attribute('Footprint')))
            count = count + 1

        print(footprints.ExportToWkt())
